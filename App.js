import * as React from 'react';
import { Text, View, StyleSheet, Alert, ImageBackground } from 'react-native';
import Constants from 'expo-constants';

// You can import from local files
import AssetExample from './components/AssetExample';

// or any pure javascript modules available in npm
import {  TextInput, Button, Card, IconButton, Avatar } from 'react-native-paper';

export default class App extends React.Component {
  image = { uri: "https://www.dicasdemulher.com.br/wp-content/uploads/2009/09/peso-ideal-1.jpg" };
  
  state = {
    altura: 0,
    peso: 0,
    imc: 0,
    legenda: 'indeterminado',
    cor: '#bdc3c7'
  };
  
  //Calculo IMC: imc = peso / altura 2 
  calcularIMC = () => {
    const resultado = this.state.peso / (this.state.altura * this.state.altura);

    this.setState({
      imc: Math.ceil(resultado),
    })

    if(resultado < 18.5 ){
      this.setState({
        legenda: 'Magreza',
        cor: '#e74c3c'
      });
    }else if (resultado >= 18.5 && resultado < 25 ){
      this.setState({
        legenda: 'Normal',
        cor: '#2ecc71'
      });
    }else if (resultado >= 25 && resultado < 29.9){
      this.setState({
        legenda: 'Sobrepeso',
        cor: '#f39c12'
      });
    }else if(resultado >= 30.0 && resultado < 39.9){
      this.setState({
        legenda: 'Obesidade',
        cor: '#e67e22'
      });
    }else if(resultado >= 40){
      this.setState({
        legenda: 'Obesidade Grave',
        cor: '#e74c3c'
      })
    }
  }

   render(){
      return (
        <ImageBackground source={this.image} resizeMode="cover" style={styles.image}>
        <View style={styles.app}>
            
            <Text style={styles.legenda}>Calculadora de IMC</Text>
  
            <View style={[styles.painel, {backgroundColor: this.state.cor}]}>
              <Text style={styles.titulo}>{this.state.imc}</Text>
              <Text style={styles.diagnostico}>{this.state.legenda}</Text>
            </View>
            <View>
              
              <TextInput style={styles.peso}
                label = "Peso"
                onChangeText = { valor => {
                  this.setState({
                    peso: valor.replace(',','.')
                  });
                }}
              />

              <TextInput style={styles.altura}
                label = "Altura"
                onChangeText = { valor => {
                  this.setState({
                    altura: valor.replace(',','.')
                  });
                }}
              />

              <Button icon="camera" mode="contained" onPress={this.calcularIMC} style={styles.button}>
                Calcular IMC
              </Button>

            </View>
        </View>
        </ImageBackground>
          
      );
   }
}

const styles = StyleSheet.create({
  app: {
    padding: 10
  },
  button: {
    width: 200,
    padding: 6,
    alignSelf: 'center'
  },
  image : {
    flex: 1,
    justifyContent: "center",
  },
  legenda: {
    textAlign: 'center',
    fontWeight: 'bold',
    // backgroundColor: '#f1c40f',
    borderRadius: 20, 
    padding: 15,
    fontSize: 30,
    color: '#2c3e50'
  },
  titulo: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: 'bold',
  },
  painel: {
    padding: 8,
    alignSelf: 'center',
    width: 200,
    marginVertical: 10,
    borderRadius: 20,
  },
  diagnostico: {
    textAlign: 'center',
    fontSize: 16
  },
  peso: {
    // height: 40,
    margin: 12,
    padding: 15,
    backgroundColor: '#ecf0f1'
  },
  altura: {
    // height: 40,
    margin: 12,
    padding: 15,
    backgroundColor: '#ecf0f1'
  },
});
